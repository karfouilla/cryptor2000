<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr" sourcelanguage="en">
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="30"/>
        <location filename="main.cpp" line="42"/>
        <location filename="main.cpp" line="59"/>
        <source>Argument error</source>
        <translation>Erreur d&apos;arguments</translation>
    </message>
    <message>
        <location filename="main.cpp" line="30"/>
        <source>The &apos;-i&apos; argument must be following to input file
Argument ignored</source>
        <translation>L&apos;argument &apos;-i&apos; doit être suivit du fichier d&apos;entré
Argument ignoré</translation>
    </message>
    <message>
        <location filename="main.cpp" line="42"/>
        <source>The &apos;-o&apos; argument must be following to output file
Argument ignored</source>
        <translation>L&apos;argument &apos;-o&apos; doit être suivit du fichier de sortie
Argument ignoré</translation>
    </message>
    <message>
        <location filename="main.cpp" line="59"/>
        <source>&apos;%1&apos; argument isn&apos;t know
Argument ignored</source>
        <translation>L&apos;argument &apos;%1&apos; est inconue
Argument ignoré</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <source>File to crypt</source>
        <translation>Fichier à crypter</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <location filename="main.cpp" line="67"/>
        <location filename="main.cpp" line="69"/>
        <source>All File(*)</source>
        <translation>Tout les fichier(*)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="67"/>
        <source>File to decrypt</source>
        <translation>Fichier à décrypter</translation>
    </message>
    <message>
        <location filename="main.cpp" line="69"/>
        <source>File to (de)crypt</source>
        <translation>Fichier à (dé)crypter</translation>
    </message>
    <message>
        <location filename="main.cpp" line="103"/>
        <location filename="main.cpp" line="108"/>
        <source>Output file</source>
        <translation>Fichier de sortie</translation>
    </message>
    <message>
        <location filename="main.cpp" line="103"/>
        <source>Cryptor 2000 File(*.cr2)</source>
        <translation>Fichier Cryptor 2000(*.cr2)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="115"/>
        <source>Opening file</source>
        <translation>Ouverture du fichier</translation>
    </message>
    <message>
        <location filename="main.cpp" line="115"/>
        <source>Unable to open file &apos;%1&apos;</source>
        <translation>Impossible d&apos;ouvrir le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="121"/>
        <source>Opening output file</source>
        <translation>Ouverture du fichier de sortie</translation>
    </message>
    <message>
        <location filename="main.cpp" line="121"/>
        <source>Unable to open output file &apos;%1&apos;</source>
        <translation>Impossible d&apos;ouvrire le fichier de sortie &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="126"/>
        <source>Password</source>
        <translation>Mots de passe</translation>
    </message>
    <message>
        <location filename="main.cpp" line="126"/>
        <source>Password: </source>
        <translation>Mots de passe: </translation>
    </message>
</context>
</TS>
