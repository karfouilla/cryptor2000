#-------------------------------------------------
#
# Project created by QtCreator 2014-12-08T16:55:39
#
#-------------------------------------------------

QT       += core gui

TARGET = Cryptor2000
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
    cryptor.cpp

HEADERS  += dialog.h \
    cryptor.h

TRANSLATIONS = en.ts fr.ts

RC_FILE = main.rc
