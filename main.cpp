////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Cryptor2000.
//
// Cryptor2000 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cryptor2000 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Cryptor2000.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtGui/QApplication>
#include <QMessageBox>
#include <QFileDialog>
#include <QInputDialog>
#include <QCryptographicHash>
#include <QTranslator>
#include <QLocale>
#include "cryptor.h"
#include "dialog.h"

int main(int argc, char *argv[])
{
    //Initialisation
    QApplication app(argc, argv);

    QTranslator translator;
    QString locale = QLocale::system().name().section('_', 0, 0);
    translator.load(locale);
    app.installTranslator(&translator);

    QString fname = "";
    QString foutname = "";
    bool crypter = false;
    bool uncrypter = false;
    bool autofoutname = false;
    //Lecture des arguments
    for(int i=1; i<argc; i++)
    {
        if(QString(argv[i]) == "-i")
        {
            if(i+1 < argc)
            {
                i++;
                fname = argv[i];
            }
            else
            {
                QMessageBox::warning(0, QObject::tr("Argument error"), QObject::tr("The '-i' argument must be following to input file\nArgument ignored"));
            }
        }
        else if(QString(argv[i]) == "-o")
        {
            if(i+1 < argc)
            {
                i++;
                foutname = argv[i];
            }
            else
            {
                QMessageBox::warning(0, QObject::tr("Argument error"), QObject::tr("The '-o' argument must be following to output file\nArgument ignored"));
            }
        }
        else if(QString(argv[i]) == "-a")
        {
            autofoutname = true;
        }
        else if(QString(argv[i]) == "-c")
        {
            crypter = true;
        }
        else if(QString(argv[i]) == "-u")
        {
            uncrypter = true;
        }
        else
        {
            QMessageBox::warning(0, QObject::tr("Argument error"), QObject::tr("'%1' argument isn't know\nArgument ignored").arg(argv[i]));
        }
    }
    //Demande des noms fichiers(si besoin)
    if(fname.isEmpty())
    {
        if(crypter)
            fname = QFileDialog::getOpenFileName(0, QObject::tr("File to crypt"), QString(), QObject::tr("All File(*)"));
        else if(uncrypter)
            fname = QFileDialog::getOpenFileName(0, QObject::tr("File to decrypt"), QString(), QObject::tr("All File(*)"));
        else
            fname = QFileDialog::getOpenFileName(0, QObject::tr("File to (de)crypt"), QString(), QObject::tr("All File(*)"));
        if(fname.isEmpty())
            return -6;
    }
    if(foutname.isEmpty())
    {
        if(uncrypter)
        {
            int idx = fname.lastIndexOf(".cr2");
            if(idx != -1)
                foutname = fname.left(idx);
            else
                foutname = fname;
        }
        else if(crypter)
        {
            foutname = fname+".cr2";
        }
        else
        {
            int idx = fname.lastIndexOf(".cr2");
            if(idx != -1)
            {
                foutname = fname.left(idx);
                uncrypter = true;
            }
            else
            {
                foutname = fname+".cr2";
                crypter = true;
            }
        }
        if(!autofoutname)
        {
            if(crypter)
            {
                foutname = QFileDialog::getSaveFileName(0, QObject::tr("Output file"), foutname, QObject::tr("Cryptor 2000 File(*.cr2)"));
                if(foutname.isEmpty())
                    return -4;
                if(foutname.lastIndexOf(".cr2") == -1)
                    foutname += ".cr2";
            }
            else
            {
                foutname = QFileDialog::getSaveFileName(0, QObject::tr("Output file"), foutname);
                if(foutname.isEmpty())
                    return -5;
            }
        }
    }
    //Demande du mots de passe
    bool ok = false;
    QString premdp = QInputDialog::getText(0, QObject::tr("Password"), QObject::tr("Password: "), QLineEdit::Normal, QString(), &ok);

    if(!ok)
        return -3;

    //Ouverture des fichiers
    QFile f(fname);
    if(!f.open(QIODevice::ReadOnly))
    {
        QMessageBox::warning(0, QObject::tr("Opening file"), QObject::tr("Unable to open file '%1'").arg(fname));
        return -1;
    }
    QFile fout(foutname);
    if(!fout.open(QIODevice::WriteOnly))
    {
        QMessageBox::warning(0, QObject::tr("Opening output file"), QObject::tr("Unable to open output file '%1'").arg(foutname));
        return -2;
    }

    //Lancement du processus
    cryptor c(premdp, f, fout);
    c.start();

    Dialog w(&c);
    w.exec();

    return 0;
}
