#ifndef CRYPTOR_H
#define CRYPTOR_H
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Cryptor2000.
//
// Cryptor2000 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cryptor2000 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Cryptor2000.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QThread>
#include <QFile>

class cryptor : public QThread
{
    Q_OBJECT

    public:
        cryptor(QString& premdp, QFile& f, QFile& fout);
        qint64 getProgress();
        qint64 getProgressmax();
    public slots:
        void endapp();
        void callquit();
    private:
        QString& premdp;
        QFile& f;
        QFile& fout;

        qint64 progress;
        qint64 progressmax;

        bool quit;

        void run();
};

#endif // CRYPTOR_H
