////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Cryptor2000.
//
// Cryptor2000 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cryptor2000 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Cryptor2000.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "cryptor.h"
#include <QCryptographicHash>
#include <QDataStream>
#include <QApplication>

cryptor::cryptor(QString& _premdp, QFile& _f, QFile& _fout):
        premdp(_premdp),
        f(_f),
        fout(_fout),
        quit(false)
{
    progressmax = f.size();
    progress = 0;
}
void cryptor::run()
{
    connect(this, SIGNAL(finished()), this, SLOT(endapp()));
    premdp = QString("\xcd\x01\xff\x0e\x1c\xdc\x1f")+premdp+QString("\x1f\x01\xcd\x1c\x0e\xff\xdc");

    QByteArray mdp = QCryptographicHash::hash(premdp.toAscii(), QCryptographicHash::Md5);
    mdp.append("\x02");
    QByteArray mdpadds[40];
    unsigned char ads[40] = {
    0x10, 0x20, 0x30, 0x40, 0x50,
    0x01, 0x03, 0x05, 0x07, 0x09,
    0x0a, 0x08, 0x06, 0x04, 0x02,
    0x11, 0x13, 0x15, 0x17, 0x19,
    0x1a, 0x18, 0x16, 0x14, 0x12,
    0x21, 0x23, 0x25, 0x27, 0x29,
    0x2a, 0x28, 0x26, 0x24, 0x22,
    0x8a, 0x88, 0x86, 0x84, 0xd2 };
    for(QByteArray::iterator i=mdp.begin(); i!=mdp.end(); i++)
    {
        for(int j=0; j<40; j++)
            mdpadds[j].append((*i)^ads[j]);
    }
    for(int j=0; j<40; j++)
        mdp.append(mdpadds[j]);

    QDataStream stream(&f);
    QDataStream outstream(&fout);

    int i=0;
    while(!stream.atEnd() && !quit)
    {
        quint8 bit;
        stream >> bit;

        bit = bit^mdp[i];

        outstream << bit;
        progress++;
        i++;
        if(i>=mdp.size())
            i=0;
    }
    f.close();
    fout.close();
}

qint64 cryptor::getProgress()
{
    return progress;
}

qint64 cryptor::getProgressmax()
{
    return progressmax;
}

void cryptor::endapp()
{
    qApp->exit();
}

void cryptor::callquit()
{
    quit = true;
}
