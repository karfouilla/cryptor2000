////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Cryptor2000.
//
// Cryptor2000 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cryptor2000 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Cryptor2000.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "dialog.h"
#include <QApplication>

Dialog::Dialog(cryptor *_c)
    : c(_c)
{
    t = new QTimer;
    connect(t, SIGNAL(timeout()), this, SLOT(update()));
    connect(this, SIGNAL(canceled()), c, SLOT(callquit()));
    t->start(250);
}

Dialog::~Dialog()
{
}

void Dialog::update()
{
    if(c->getProgressmax() == 0)
        setValue(0);
    else
    {
        setValue(c->getProgress()*100/c->getProgressmax());
    }
    if(value() != 100)
        t->start(250);
}
