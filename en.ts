<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en" sourcelanguage="en">
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="30"/>
        <location filename="main.cpp" line="42"/>
        <location filename="main.cpp" line="59"/>
        <source>Argument error</source>
        <translation>Argument error</translation>
    </message>
    <message>
        <location filename="main.cpp" line="30"/>
        <source>The &apos;-i&apos; argument must be following to input file
Argument ignored</source>
        <translation>The &apos;-i&apos; argument must be following to input file
Argument ignored</translation>
    </message>
    <message>
        <location filename="main.cpp" line="42"/>
        <source>The &apos;-o&apos; argument must be following to output file
Argument ignored</source>
        <translation>The &apos;-o&apos; argument must be following to output file
Argument ignored</translation>
    </message>
    <message>
        <location filename="main.cpp" line="59"/>
        <source>&apos;%1&apos; argument isn&apos;t know
Argument ignored</source>
        <translation>&apos;%1&apos; argument isn&apos;t know
Argument ignored</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <source>File to crypt</source>
        <translation>File to crypt</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <location filename="main.cpp" line="67"/>
        <location filename="main.cpp" line="69"/>
        <source>All File(*)</source>
        <translation>All File(*)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="67"/>
        <source>File to decrypt</source>
        <translation>File to decrypt</translation>
    </message>
    <message>
        <location filename="main.cpp" line="69"/>
        <source>File to (de)crypt</source>
        <translation>File to (de)crypt</translation>
    </message>
    <message>
        <location filename="main.cpp" line="103"/>
        <location filename="main.cpp" line="108"/>
        <source>Output file</source>
        <translation>Output file</translation>
    </message>
    <message>
        <location filename="main.cpp" line="103"/>
        <source>Cryptor 2000 File(*.cr2)</source>
        <translation>Cryptor 2000 File(*.cr2)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="115"/>
        <source>Opening file</source>
        <translation>Opening file</translation>
    </message>
    <message>
        <location filename="main.cpp" line="115"/>
        <source>Unable to open file &apos;%1&apos;</source>
        <translation>Unable to open file &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="121"/>
        <source>Opening output file</source>
        <translation>Opening output file</translation>
    </message>
    <message>
        <location filename="main.cpp" line="121"/>
        <source>Unable to open output file &apos;%1&apos;</source>
        <translation>Unable to open output file &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="126"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="main.cpp" line="126"/>
        <source>Password: </source>
        <translation>Password: </translation>
    </message>
</context>
</TS>
