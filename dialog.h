#ifndef DIALOG_H
#define DIALOG_H
////////////////////////////////////////////////////////////////////////////////
//
// This file is part of Cryptor2000.
//
// Cryptor2000 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cryptor2000 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Cryptor2000.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include <QtGui/QProgressDialog>
#include <QTimer>
#include "cryptor.h"

class Dialog : public QProgressDialog
{
    Q_OBJECT

public:
    Dialog(cryptor *_c);
    ~Dialog();

public slots:
    void update();

private:
    QTimer* t;
    cryptor* c;
};

#endif // DIALOG_H
